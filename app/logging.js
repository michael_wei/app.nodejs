'use strict';
var path = require('path'),
  log4js = require('log4js'),
  config = require('./config'),
  _ = require('lodash');

var Logger = function () {
  log4js.configure(path.join(config.root, '/app/envs/', config.env + '.log4js.json'));

  this.getLogger = function (category) {
    if (_.isString(category) && !_.isEmpty(category)) {
      return log4js.getLogger(category);
    }
    throw 'category can not be empty';
  };

  this.getDefaultLogger = function () {
    return log4js.getDefaultLogger();
  };
};

module.exports = new Logger();
