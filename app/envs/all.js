var path = require('path');

module.exports = {
  env: process.env.NODE_ENV,
  root: path.normalize(__dirname + '/../..'),
  port: process.env.PORT || 3000,
  db: process.env.MONGOHQ_URL,
  cookieAge: 120 * 60 * 1000
};
