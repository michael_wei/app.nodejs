'use strict';
var base = require('./base');

var IndexController = function () {
  base.call(this);

  this.getDefault = function (req, res) {
    /*jshint unused: false */
    res.render('app/index');
  };
};
IndexController.prototype = Object.create(base.prototype);
IndexController.prototype.constructor = IndexController;

module.exports = new IndexController();

