var _ = require('lodash');

module.exports = _.extend(
  require('./envs/all.js'),
  require('./envs/' + process.env.NODE_ENV + '.js') || {});
