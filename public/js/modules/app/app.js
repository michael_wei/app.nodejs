require('source-map-support').install();
require('jquery');
require('modernizr');

var angular = require('angular');
var ngModule = angular.module('app',
  [
    'ui.router',
    'ngResource',
    'ngSanitize',
    'ui.bootstrap',
    require('../common').name
  ])
  .config(function ($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
  })
  .config(function($locationProvider){
    $locationProvider.html5Mode(true);
  });

//require('./bootstrap')(ngModule);

module.export = ngModule;
