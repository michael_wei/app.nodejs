'use strict';

var angular = require('angular');
var ngModule = angular.module('app.common', [
  'ui.router',
  'ngResource',
  'ngSanitize',
  'ngLocale',
  'cancer.template'
]).value('version', '0.1');

module.exports = ngModule;