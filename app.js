'use strict';
var config = require('./app/config');

if (config.env === 'local') {
  require('./app/startup')();
} else {
  require('./app/cluster')({}, require('./app/startup'));
}