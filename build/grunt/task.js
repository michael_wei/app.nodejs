'use strict';
var path = require('path');

module.exports = function (grunt) {
  grunt.registerTask('build:dev',
    ['build:tpl', 'build:app', 'less:dev']);

  grunt.registerTask('test:dev',
    ['mochaTest:dev', 'karma:dev']);

  return {};
};