'use strict';
var path = require('path');
var _ = require('lodash');
var BrowserifyCmd = require('../modules/BrowserifyCmd');

module.exports = function (grunt, root) {
  var pkg = require('./package')(grunt, root).pkg;
  var browserify = new BrowserifyCmd(pkg.browser, pkg['browserify-shim']);

  {
    grunt.loadNpmTasks('grunt-mkdir');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-run');
  }

  {
    grunt.registerTask('build:app',
        ['mkdir:browserify', 'copy:browserify', 'run:browserify-app', 'clean:browserify']);

    grunt.registerTask('build:libs',
        ['mkdir:browserify', 'copy:browserify', 'run:browserify-libs', 'clean:browserify']);

    grunt.registerTask('build:app-coverage',
        ['mkdir:browserify', 'copy:browserify', 'run:browserify-app-coverage', 'clean:browserify']);

    grunt.registerTask('build:tpl',
        ['mkdir:browserify', 'html2js:main', 'run:browserify-tpl']);
  }


  return {
    clean: {
      libs: {
        src: path.join('./<%= configs.paths.jsOutput %>', 'libs.js')
      },
      app: {
        src: path.join('./<%= configs.paths.jsOutput %>', 'app.js')
      },
      browserify: {
        src: './<%= configs.paths.tempLibs %>'
      }
    },
    mkdir: {
      browserify: {
        options: {
          create: ['<%= configs.paths.tempLibs %>']
        }
      }
    },
    copy: {
      browserify: {
        expand: true,
        flatten: true,
        src: _.values(pkg.sources),
        dest: '<%= configs.paths.tempLibs %>'
      }
    },
    run: {
      options: {
        failOnError: true,
        cwd: root
      },
      'browserify-libs': {
        exec: browserify.getCmd(false, './<%= configs.paths.jsOutput %>/libs.js')
      },
      'browserify-tpl': {
        exec: browserify.getCmd(true, './<%= configs.paths.jsOutput %>/tpl.js', ['./<%= configs.paths.jsOutput %>/tpl.raw.js'])
      },
      'browserify-app': {
        exec: browserify.getCmd(true, './<%= configs.paths.jsOutput %>/app.js', ['./<%= configs.paths.js %>/app/app.js'])
      },
      'browserify-app-coverage': {
        exec: browserify.getCmd(true, './<%= configs.paths.jsOutput %>/app.coverage.js', ['./<%= configs.paths.js %>/app/app.js'], ['-t browserify-istanbul'])
      }
    }
  };
};