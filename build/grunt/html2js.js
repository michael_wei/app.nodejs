'use strict';
var path = require('path');

module.exports = function (grunt) {
  grunt.loadNpmTasks('grunt-html2js');
  return {
    html2js: {
      options: {
        module: 'app.template',
        useStrict: true,
        rename: function (moduleName) {
          return path.basename(moduleName).replace('.jade', '');
        },
        singleModule: true
      },
      main: {
        src: [path.join('./<%= configs.paths.js %>', '/**/*.jade')],
        dest: path.join('./<%= configs.paths.jsOutput %>', 'tpl.raw.js')
      }
    }
  };
};