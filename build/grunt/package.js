'use strict';
var path = require('path');

module.exports = function (grunt, root) {
  return {
    pkg: grunt.file.readJSON(path.resolve(root, 'package.json'))
  };
};