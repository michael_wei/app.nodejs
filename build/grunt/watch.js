'use strict';

module.exports = function (grunt) {
  grunt.loadNpmTasks('grunt-contrib-watch');
  return {
    watch: {
      build: {
        files: ['<%= configs.sources.js %>'],
        tasks: ['run:browserify-app']
      },
      karma: {
        files: ['<%= configs.sources.test %>'],
        tasks: ['karma:watch:run']
      }
    }
  };
};