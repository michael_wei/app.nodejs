'use strict';

module.exports = function (grunt) {
  grunt.loadNpmTasks('grunt-contrib-uglify');
  return {
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' + '<%= grunt.template.today("yyyy-mm-dd") %> */'
      },
      app: {
        options: {
          sourceMap: true,
          sourceMapName: './<%= configs.paths.jsOutput %>/app.map'
        },
        files: {
          './<%= configs.paths.jsOutput %>/app.min.js': ['./<%= configs.paths.jsOutput %>/app.js']
        }
      },
      tpl: {
        options: {
          sourceMap: true,
          sourceMapName: './<%= configs.paths.jsOutput %>/tpl.map'
        },
        files: {
          './<%= configs.paths.jsOutput %>/tpl.min.js': ['./<%= configs.paths.jsOutput %>/tpl.js']
        }
      },
      libs: {
        files: {
          './<%= configs.paths.jsOutput %>/libs.min.js': ['./<%= configs.paths.jsOutput %>/libs.js']
        }
      }
    }
  };
};