'use strict';
var configs = require('./config').configs;

module.exports = function (grunt) {
  grunt.loadNpmTasks('grunt-contrib-sass');

  grunt.registerTask('check:sass',
    function () {
      var isComplete =
        grunt.file.exists(configs.paths.sass, '_variables.scss') &&
        grunt.file.exists(configs.paths.sass, 'main.scss');

      if (!isComplete) {
        grunt.log.error('sass files are missing.');
      }
    });

  grunt.registerTask('build:sass:dev', [
    'build:css',
    'check:sass',
    'sass:dev'
  ]);

  grunt.registerTask('build:sass:dist', [
    'build:css',
    'check:sass',
    'sass:dist'
  ]);

  return {
    sass: {
      dist: {
        options: {
          sourcemap: 'auto',
          noCache: true,
          style: 'compressed'
        },
        files: {'<%= configs.paths.cssOutput %>/app.css': '<%= configs.paths.sass %>/main.scss'}
      },
      dev: {
        options: {
          sourcemap: true,
          noCache: true
        },
        files: {'<%= configs.paths.cssOutput %>/app.css': '<%= configs.paths.sass %>/main.scss'}
      }
    }
  };
};