'use strict';
var _ = require('lodash');
var path = require('path');
var configs = require('./config').configs;

module.exports = function (grunt) {
  grunt.loadNpmTasks('grunt-mocha-test');
  grunt.loadNpmTasks('grunt-mocha-istanbul');

  function toMochaTest(files) {
    return _.map(files, function (file) {
      return file.folder + '/**/' + file.mask;
    });
  }

  function toMochaIstanbul(files) {
    return _.map(files, function (file) {
      return file.folder;
    });
  }

  return {
    mochaTest: {
      dev: {
        options: {
          reporter: 'spec',
          quiet: false
        },
        src: toMochaTest(configs.sources.test)
      }
    },
    'mocha_istanbul': {
      coverage: {
        src: toMochaIstanbul(configs.sources.test),
        options: {
          mask: '**/*.spec.js',
          reportFormats: ['teamcity', 'html', 'text']
        }
      }
    }
  };
};