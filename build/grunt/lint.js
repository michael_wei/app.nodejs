'use strict';
var _ = require('lodash');
var path = require('path');
var configs = require('./config').configs;

module.exports = function (grunt) {
  grunt.loadNpmTasks('grunt-contrib-jshint');

  grunt.registerTask('lint',
    ['jshint']);

  var tests = _(configs.sources.test)
    .map(function(test){
      return path.join(test.folder, test.mask);
    }).value();

  return {
    jshint: {
      options: {
        jshintrc: true
      },
      all: {
        src: _.flatten([
          '<%= configs.sources.node %>',
          '<%= configs.sources.js %>',
          tests,
          '<%= configs.sources.build %>'
        ])
      }
    }
  };
};