'use strict';
var path = require('path');

module.exports = function (grunt) {
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');

  grunt.registerTask('build:css', [
    'clean:css',
    'copy:font-awesome',
    'copy:leaflet'
  ]);

  return {
    clean: {
      css: {
        src: [
          path.join('<%= configs.paths.cssOutput %>', 'app.css'),
          path.join('<%= configs.paths.cssOutput %>', 'app.css.map')
        ]
      }
    },
    copy: {
      'font-awesome': {
        expand: true,
        cwd: './bower_components/fontawesome',
        src: ['fonts/**'],
        dest: '<%= configs.paths.cssOutput %>'
      },
      leaflet: {
        expand: true,
        cwd: './bower_components/leaflet/dist/images',
        src: ['**'],
        dest: path.join('<%= configs.paths.images %>', 'leaflet')
      }
    }
  };
};