'use strict';
var configs = require('./config').configs;

module.exports = function (grunt) {
  grunt.loadNpmTasks('grunt-contrib-less');

  grunt.registerTask('check:less',
    function () {
      var isComplete =
        grunt.file.exists(configs.paths.less, 'variables.less') &&
        grunt.file.exists(configs.paths.less, 'main.less');

      if (!isComplete) {
        grunt.log.error('less files are missing.');
      }
    });

  grunt.registerTask('build:less:dev', [
    'build:css',
    'check:less',
    'less:dev'
  ]);

  grunt.registerTask('build:less:dist', [
    'build:css',
    'check:less',
    'less:dist'
  ]);

  return {
    less: {
      dist: {
        options: {
          compress: true,
          cleancss: true,
          banner: '/* Generated at <%= grunt.template.today("yyyy-mm-dd hh:mm:ss") %> */\n'
        },
        files: {'<%= configs.paths.cssOutput %>/app.css': '<%= configs.paths.less %>/main.less'}
      },
      dev: {
        options: {
          compress: false,
          cleancss: false,
          sourceMap: true,
          outputSourceFiles: true,
          sourceMapURL: 'app.css.map',
          sourceMapFilename: '<%= configs.paths.cssOutput %>/app.css.map'
        },
        files: {'<%= configs.paths.cssOutput %>/app.css': '<%= configs.paths.less %>/main.less'}
      }
    }
  };
};