'use strict';
var _ = require('lodash');

module.exports = function (grunt, root) {
  var config = _({})
    .merge(require('./config'))
    .merge(require('./package')(grunt, root))
    .merge(require('./html2js')(grunt, root))
    .merge(require('./browserify')(grunt, root))
    .merge(require('./lint')(grunt, root))
    .merge(require('./css')(grunt, root))
    .merge(require('./less')(grunt, root))
    .merge(require('./sass')(grunt, root))
    .merge(require('./mocha')(grunt, root))
    .merge(require('./karma')(grunt, root))
    .merge(require('./uglify')(grunt, root))
    .merge(require('./watch')(grunt, root))
    .merge(require('./task')(grunt, root))
    .value();

  grunt.initConfig(config);
};