'use strict';

module.exports = function (grunt) {
  grunt.loadNpmTasks('grunt-karma');
  return {
    karma: {
      options: {
        configFile: 'tests/karma/karma.conf.js',
        singleRun: true
      },
      dev: {
        options: {
          singleRun: true,
          reporters: ['mocha'],
          browsers: ['PhantomJS']
        }
      },
      allCoverage: {
        options: {
          singleRun: true,
          reporters: ['progress', 'teamcity', 'coverage'],
          files: [
            'public/js/build/libs.js',
            'public/js/build/tpl.js',
            'public/js/build/app.coverage.js',

            'bower_components/angular-mocks/angular-mocks.js',
            'tests/karma/**/*.spec.js'
          ],
          browsers: ['PhantomJS']
        }
      },
      watch: {
        autoWatch: false,
        singleRun: false,
        background: true
      }
    }
  };
};