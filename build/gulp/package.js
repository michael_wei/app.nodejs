'use strict';
var path = require('path');

module.exports = function (root) {
  return {
    pkg: require(path.resolve(root, 'package.json'))
  };
};