module.exports = {
  configs: {
    sources: {
      node: 'app/**/*.js',
      js: 'public/js/modules/**/*.js',
      jade: 'views/**/*.jade',
      css: ['public/styles/**/*.css'],
      test: [
        {
          folder: 'tests/mocha',
          mask: '*.spec.*'
        },
        {
          folder: 'app/modules/grunt-browserify',
          mask: '*.spec.*'
        },
        {
          folder: 'app/modules/smart-forEach',
          mask: '*.spec.*'
        }
      ],
      build: 'build/**/*.js'
    },
    paths: {
      tempLibs: 'build/temp',
      js: 'public/js/modules',
      jsOutput: 'public/js',
      less: 'public/styles/less',
      sass: 'public/styles/sass',
      cssOutput: 'public/styles',
      images: 'public/images',
      coverage: 'coverage'
    }
  }
};